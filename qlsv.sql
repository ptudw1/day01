CREATE DATABASE qlsv;
USE qlsv;
CREATE TABLE DMKHOA
(
    	MaKH varchar(6) NOT NULL, 
	TenKhoa varchar(30), 
	PRIMARY KEY(MaKH)
);


CREATE TABLE sinhvien
(
    MaSV VARCHAR(6) NOT NULL,
    HoSV VARCHAR(30) NOT NULL, 
    TenSV VARCHAR(15) NOT NULL,
    GioiTinh CHAR(1),
    NGAYSINH DATETIME NOT NULL,
    NoiSinh VARCHAR(50),
    DiaChi VARCHAR(50),
    MaKH VARCHAR(6) NOT NULL,
    HocBong INT,
    PRIMARY KEY(MaSV)
);
ALTER TABLE sinhvien ADD FOREIGN KEY(MaKH) REFERENCES dmkhoa(MaKH);

INSERT INTO dmkhoa(MaKH,TenKhoa)
VALUES("KHAVAN","Khoa anh van");
INSERT INTO dmkhoa(MaKH,TenKhoa)
VALUES("KHCNTT","Khoa cong nghe thong tin");
INSERT INTO dmkhoa(MaKH,TenKhoa)
VALUES("KHDTVT","Khoa dien tu vien thong");
INSERT INTO dmkhoa(MaKH,TenKhoa)
VALUES("KHQTKD","Khoa quan tri khinh doanh");

INSERT INTO sinhvien(MaSV,HoSV,TenSV,GioiTinh,NGAYSINH,NoiSinh,DiaChi,MaKH,HocBong)
VALUES("CNT001","Tran Minh","Son","1","1985-05-01","Quang Ngai","Quang Ngai","KHCNTT","");
INSERT INTO sinhvien(MaSV,HoSV,TenSV,GioiTinh,NGAYSINH,NoiSinh,DiaChi,MaKH,HocBong)
VALUES("CNT002","Nguyen Quoc","Bao","1","1986-05-16","Quang Nam","Quang Nam","KHCNTT","1");
INSERT INTO sinhvien(MaSV,HoSV,TenSV,GioiTinh,NGAYSINH,NoiSinh,DiaChi,MaKH,HocBong)
VALUES("QKD003","Pham Anh","Tung","1","1983-12-20","Ha Noi","Ha Noi","KHQTKD","");
INSERT INTO sinhvien(MaSV,HoSV,TenSV,GioiTinh,NGAYSINH,NoiSinh,DiaChi,MaKH,HocBong)
VALUES("QKD004","Bui Thi Anh","Thu","2","1985-02-01","Ha Noi","Ha Noi","KHQTKD","");
INSERT INTO sinhvien(MaSV,HoSV,TenSV,GioiTinh,NGAYSINH,NoiSinh,DiaChi,MaKH,HocBong)
VALUES("GVT005","Nguyen Minh","Son","1","1987-07-03","Ha Nam","Ha Nam","KHDTVT","");
INSERT INTO sinhvien(MaSV,HoSV,TenSV,GioiTinh,NGAYSINH,NoiSinh,DiaChi,MaKH,HocBong)
VALUES("CNT006","Nguyen Thi","Lam","2","1984-11-11","TP Ho Chi Minh","TP HCM","KHCNTT","1");



